package model;
import services.InitiateData;
import java.util.List;

public class Company {

    private String name;
    private List<Employee> employeeList = InitiateData.initEmployee();

    public Company() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Company(String name) {
        this.name = name;
    }
    //calculates the summary of the employee (monthly)salaries and multiplies it with 12(months)
    public double calculateYearlyExpanses(){
        double summary = 0;
        for (int i = 0; i < this.employeeList.size(); i++){
            summary += employeeList.get(i).getSalary();
        }
        return summary * 12;
    }
    //takes name and bonus inputs, creates a new employee and adds him to the employeeList
    public void hireNewEmployee(String name, double bonus){

        Employee employee = new Employee(name, bonus);
        employeeList.add(employee);

        System.out.println(employeeList);
    }

    //takes an id input and removes from the list the employee with this id
    public void fireEmployee(int id){
        employeeList.removeIf(employee -> employee.getId() == id);
        System.out.println(employeeList);
    }
    /*takes id and bonus inputs and foreach employee in the list, if
    the employee with this id is a Developer, is being promoted to Manager
     */
    public void promoteDeveloper(int id, double bonus){
        for (Employee employee :  employeeList) {
            if ((employee.getId() == id) && (employee.getType() == "Developer" )){
                employee.setBonus(bonus);
                employee.setType();
                employee.setSalary(bonus);
                System.out.println(employeeList);
                return;
            }
        }
    }
    /*foreach employee in the list with the given id, sets new information
    (name and bonus) */
    public void editEmployeeInformation(int id, String name, double bonus){
        for (Employee employee :  employeeList) {
            if (employee.getId() == id){
                employee.setName(name);
                employee.setBonus(bonus);
                employee.setType();
                employee.setSalary(bonus);
                System.out.println(employeeList);
                return;
            }
        }

    }
}
