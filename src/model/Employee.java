package model;

public class Employee {

    private int id;
    private String name;
    private String type;
    private double salary = 500;
    private double bonus;
    public static int counter;

    public Employee() {
        counter++;
        id = counter;
    }

    public int getId() { return id; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getType() { return type; }

    //sets the type of employee depending on the bonus input
    public void setType() {
        if (bonus == 0){
            this.type = "Developer";
        }else if((bonus >= 2)&& (bonus <= 4)){
            this.type = "Manager";
        }else {
            throw new IllegalArgumentException("Invalid bonus");
        }
    }

    public double getSalary() { return salary; }

    //calculates the salary depending on the bonus
    public void setSalary(double bonus) {
        salary += salary * bonus / 100;
    }

    public double getBonus() {
        return bonus;
    }

    //checks if the bonus input is valid
    public void setBonus(double bonus) {
        if ((bonus == 0)||((bonus >= 2)&& (bonus <= 4))) {
            this.bonus = bonus;
        }else {
            throw new IllegalArgumentException("Invalid bonus");
        }
    }

    public Employee(String name, double bonus) {
        counter++;
        id = counter;
        this.name = name;
        if (bonus == 0){
            this.type = "Developer";
        }else if((bonus >= 2)&& (bonus <= 4)){
            this.type = "Manager";
        }else {
            throw new IllegalArgumentException("Invalid bonus");
        }
        salary += salary * bonus / 100;
        if ((bonus == 0)||((bonus >= 2)&& (bonus <= 4))) {
            this.bonus = bonus;
        }else {
            throw new IllegalArgumentException("Invalid bonus");
        }
    }
    @Override
    public String toString() {
        return "ID : " + id + ", Name : " + name + ", Type : " + type + ", Bonus : " + bonus + ", Salary : " + salary + '\n';

    }
}
