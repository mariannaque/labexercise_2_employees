package services;
import model.Employee;
import java.util.ArrayList;
import java.util.List;

public class InitiateData {

    public static List<Employee> initEmployee(){

        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(new Employee("Nikos",0));
        employeeList.add(new Employee("Yiannis",0));
        employeeList.add(new Employee("Kostas",2.2));
        employeeList.add(new Employee("Takis",2.5));
        return employeeList;
    }
}
