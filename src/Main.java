import model.Company;

import java.util.Scanner;

public class Main {

    public static void main(String args[]){

        Company company = new Company("Accenture");

        System.out.println("Welcome to Employees App!");
        Scanner scanner = new Scanner(System.in);
        String input = "";
        while (input != "end")
        {
            System.out.println("Please type a number to select an option:\n\r[1]Hire a new Employee\n\r[2]Fire an employee\n\r[3]Promote a developer\n\r[4]Edit employee information\r\n[5]Calculate yearly expanses");

            input = scanner.nextLine();
            switch (input)
            {
                case "1":
                    System.out.println("Please type Employee's name:");
                    String name = scanner.nextLine();
                    System.out.println("Enter bonus (zero for developer)");
                    String bonus = scanner.nextLine();
                    company.hireNewEmployee(name, Double.parseDouble(bonus));
                    break;
                case "2":
                    System.out.println("Please type Employee's id:");
                    String id = scanner.nextLine();
                    company.fireEmployee(Integer.parseInt(id));
                    break;
                case "3":
                    System.out.println("Please type Employee's id:");
                    String id3 = scanner.nextLine();
                    System.out.println("Enter bonus");
                    String bonus3 = scanner.nextLine();
                    company.promoteDeveloper(Integer.parseInt(id3), Double.parseDouble(bonus3));
                    break;
                case "4":
                    System.out.println("Please type Employee's id:");
                    String id4 = scanner.nextLine();
                    System.out.println("Please type Employee's name:");
                    String name4 = scanner.nextLine();
                    System.out.println("Enter bonus (zero for developer)");
                    String bonus4 = scanner.nextLine();

                    company.editEmployeeInformation(Integer.parseInt(id4),name4, Double.parseDouble(bonus4));
                    break;
                case "5":
                    System.out.println("The Company's yearly expanses are : " + company.calculateYearlyExpanses());

                    break;
                case "end":
                    break;
                default:
                    System.out.println("Invalid input");
                    break;
            }
        }
 }

}
